import { CheckCircleOutlined, DeleteOutlined, EditOutlined } from '@ant-design/icons/lib/icons';
import React, { useRef } from 'react';

import './ActionButton.css';

// Generic Button that stores the index and passes it as argument for onClick
// The Button can be a Delete, Edit or Confirm button
const ActionButton = (props) => {
    const edit = props.edit;
    const confirm = props.confirm;
    const onClick = props.onClick;
    const index = useRef(props.index);

    const click = () => {
        onClick(index.current);
    }
    if(edit){
        if(confirm){
            return (
                <CheckCircleOutlined className="ActionButton" onClick={click}/>
            );
        }
        return (
            <EditOutlined className="ActionButton" onClick={click}/>
        );
    }
    else{
        if(confirm){
            return(<></>);
        }
        return (
            <DeleteOutlined className="ActionButton" onClick={click}/>
        );
    }
}

export default ActionButton;
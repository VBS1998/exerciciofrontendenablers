import React from 'react';
import { Button, Col, Form, Input, Row } from 'antd';
import { useNavigate } from 'react-router-dom';
import './LoginScreen.css';
import api from '../api'

const LoginScreen = () => {

  const navigate = useNavigate();
  const [form] = Form.useForm();
  const [formMessage, setFormMessage] = React.useState("");

  //Method to be called when the Log In Button is pressed
  const login = (_) =>{
    const {username, password} = form.getFieldsValue(['username', 'password']);
    if(username && password){
      // Posts the username and password to get the JWT back
      api
        .post('/auth', {
          user: username,
         pass: password
        })
        .then((res) => {
          const token = res.data;
          if (token) {
            //Saves the JWT in the local storage for further use
            localStorage.setItem('token', token);
            navigate('/clients');
          }
          else {
            setFormMessage("Invalid username or password.");
          }
        })
    }
  }

  // Method to register a new user
  const register = (_) =>{
    const {username, password} = form.getFieldsValue(['username', 'password']);
    if(username && password){
      // Posts the username and password
      api
        .post('/create_user', {
          user: username,
          pass: password
        })
        .then((res) => {
          setFormMessage(res.data.message)          
        })
    }
  }

  return (
    <div className="LoginScreen">
      <Form
        name="basic"
        form={form}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        initialValues={{ remember: true }}
        autoComplete="off"
      >
        <Form.Item
          label="Username"
          name="username"
          rules={[{ required: true, message: 'Please input your username!' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[{ required: true, message: 'Please input your password!' }]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Row>
            <Col span={8}>
              <Button type="primary" htmlType="submit" onClick={login}>
                Log In
              </Button>
            </Col>
            <Col span={6}/>
            <Col span={8}>
              <Button type="primary" htmlType="submit" onClick={register}>
                Register
              </Button>
            </Col>
          </Row>
          
        </Form.Item>
        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <p>{`${formMessage}`}</p>
        </Form.Item>
      </Form>
    </div>
  )
};

export default LoginScreen;
import React, { useEffect, useRef } from 'react';
import { Button, Input, Table } from 'antd';
import './Clients.css';
import ActionButton from '../components/ActionButton';
import api from '../api';

const Clients = () => {
    const [data, setData] = React.useState([]);
    const [loading, setLoading] = React.useState(true);

    var isCreating = useRef(false);

    const [editedIndex, setEditedIndex] = React.useState(-1);
    const [isEditing, setIsEditing] = React.useState(false);

    var editedData = useRef({name: "", age: "", id: ""});

    const requestConfig = {headers: {authorization: localStorage.getItem('token')}};

    // Method to GET all clients from the database
    const getClients = () => {
        api
            .get('/clients', requestConfig)
            .then((res) => {
                const success = res.data.success;
                const message = res.data.message;

                if (success) {
                    setData(message)
                    setLoading(false)
                }
                else {
                    alert(message + '. Please refresh the page.')
                }
            })
    }

    //GET data from API
    useEffect(() => {
        getClients();
    }, []);

    // Method to update the values being edited
    const onFill = (ctx) => {
        const field = ctx.target.placeholder.toLowerCase();;

        if(field !== 'name'){
            ctx.target.value = ctx.target.value.replace(/\D/g,'');
        }

        editedData.current[field] = ctx.target.value;
    }

    // Method that handles the Edit and Confirm Buttons
    const editClient = (index) => {

        // Handles the Confirm Button
        if(isEditing){
            if(editedIndex === index){

                var aux = [...data];
                aux[index] = {...editedData.current};
                editedData.current = {name: "", age: "", id: ""};

                const id = data[index].id;

                setData(aux);
 
                setIsEditing(false);
                setEditedIndex(-1);

                if(isCreating.current){
                    // A new client should be Posted with its info
                    const newInfo = { ...aux[index] };
                    api
                        .post('/clients', newInfo, requestConfig)
                        .then((res) => {
                            if (!res.data.success) {
                                alert(res.data.message)
                                getClients(); // If fails, refresh with database content
                            }
                        })
                    
                    isCreating.current = false;
                }else{
                    // An edited client should be patched with the new Info
                    const newInfo = { ...aux[index] };
                    delete newInfo.id;
                    api
                        .patch('/clients/' + id, newInfo, requestConfig)
                        .then((res) => {
                            if (!res.data.success) {
                                alert(res.data.message)
                                getClients(); // If fails, refresh with database content
                            }
                        })
                }
                
            }
        }
        // Handles the edit button
        else{
            setIsEditing(true);
            setEditedIndex(index);

            editedData.current = {...data[index]};

            const editedClient = {
                name: <Input placeholder="Name" defaultValue={data[index].name} onChange={onFill} />,
                age: <Input placeholder="Age" defaultValue={data[index].age} onChange={onFill} />,
                id: data[index].id,
            }

            var aux = [...data];
            aux[index] = editedClient;
            setData(aux);
        }
    }

    //Handles the delete Button
    const deleteClient = (index) => {
        const clientID = data[index].id;
        const aux = [...data];
        aux.splice(index, 1);
        setData(aux);

        // Send A DELETE request to the api
        api
            .delete('/clients/' + clientID, requestConfig)
            .then((res) => {
                if(!res.data.success){
                    alert(res.data.message);
                    getClients();
                }
            });
    }

    // Handles the New Client Button
    const addClient = () => {
        if(!isEditing){
            const newClient = {
                name: <Input placeholder="Name" onChange={onFill}/>,
                age: <Input placeholder="Age" onChange={onFill} />,
                id: <Input placeholder="ID" onChange={onFill}/>,
            }
            setData(data => [...data, newClient]);
            setIsEditing(true);
            setEditedIndex(data.length);
            isCreating.current = true;
        }

    }

    // The Columns template for the table
    const columns = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
            render: text => (<b>{text}</b>),
        },
        {
            title: 'Age',
            dataIndex: 'age',
        },
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Actions',
            dataIndex: 'actions',
            key: 'actions',
            render: (_, __, index) => (
                <>
                    <ActionButton edit confirm={isEditing && index===editedIndex} onClick={editClient} index={index}/>
                    <ActionButton confirm={isEditing && index===editedIndex} onClick={deleteClient} index={index}/>
                </>
            ),
        }
    ]
    
    return (
        <>  
            <div className="Clients-Table">
                <Table loading={loading} columns={columns} dataSource={data} pagination={false}/>
            </div>
            <div className="Clients-AddButton">
                <Button type="primary" onClick={addClient}>Add Client</Button>
            </div>
        </>
    )
}

export default Clients;
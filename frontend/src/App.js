import { Routes, Route, Navigate } from 'react-router-dom';
import React from 'react';
import LoginScreen from './pages/LoginScreen';
import Clients from './pages/Clients';

const App = () => (
    <Routes>
        <Route exact path="/" element={<Navigate to='/login'/>} />
        <Route path="login" element={<LoginScreen/>} />
        <Route path="clients" element={<Clients/>} />        
    </Routes>
);

export default App;